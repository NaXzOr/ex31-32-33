namespace Ex31._4_32_33._3;

public class Person
{
    public string Name
    {
        get;
        set;
    }

    public Person(string name)
    {
        Name = name;
    }
    
}