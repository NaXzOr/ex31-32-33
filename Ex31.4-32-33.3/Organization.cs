namespace Ex31._4_32_33._3;

public class Organization
{
    public string Name
    {
        get;
        set;
    }

    public string Address
    {
        get;
        set;
    }

    public Organization(string name)
    {
        Name = name;
    }

}