namespace Ex31._4_32_33._3;

public class Academy : Organization, ISubject
{
    private List<IObserver> _students = new List<IObserver>();
    private string _message;
  
    public string Message
    {
        get
        {
            return _message;
        }
        set
        {
            _message = value;
            Notify();
        }
    }

    public Academy(string name, string address) : base (name)
    {
        Name = name;
        Address = address;
    }

    public void Attach(IObserver o)
    {
        _students.Add(o);
    }

    public void Detach(IObserver o)
    {
        _students.Remove(o);
    }

    public void Notify()
    {
        foreach (var student in _students)
        {
            student.Update();
        }
    }

}