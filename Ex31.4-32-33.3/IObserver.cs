namespace Ex31._4_32_33._3;

public interface IObserver
{
    public void Update();
}