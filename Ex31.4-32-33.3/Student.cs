namespace Ex31._4_32_33._3;

public class Student : Person, IObserver
{
    private Academy _academy;
    public string Message;

    public Student(Academy academy, string name) : base(name)
    {
        _academy = academy;
        Name = name;
    }
    
    public void Update()
    {
        Message = _academy.Message;
        Console.WriteLine($"Studerende {Name} modtog nyheden {Message} fra akademiet {_academy.Name} ved {_academy.Address}");
    }
}