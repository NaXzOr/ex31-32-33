namespace Ex31._4_32_33._3;

public interface ISubject
{
    void Attach(IObserver o);
    void Detach(IObserver o);
    void Notify();
}